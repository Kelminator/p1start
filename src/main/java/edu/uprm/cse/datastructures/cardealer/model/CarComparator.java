package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car a, Car b) {
		String car1 = a.getCarBrand() + a.getCarModel() + a.getCarModelOption();
		String car2 = b.getCarBrand() + b.getCarModel() + b.getCarModelOption();
		return car1.compareToIgnoreCase(car2);
//	    if(a.getCarBrand().compareToIgnoreCase((b.getCarBrand()))==0){
//	        if(a.getCarModel().compareToIgnoreCase(b.getCarModel())==0){
//	                return a.getCarModelOption().compareToIgnoreCase(b.getCarModelOption());
//            }
//	        else{ s
//	            return a.getCarModel().compareToIgnoreCase(b.getCarModel());
//            }
//        }
//	    else{
//	        return a.getCarBrand().compareToIgnoreCase(b.getCarBrand());
//        }

	}

}
