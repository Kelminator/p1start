package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.util.SortedList;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class SortedCircularDoublyLinkedList<E> implements SortedList<E> {
	/**
	 * @author kelminator
	 *The first class I made was the Node<E> class 
	 *in it I created the node constructor,
	 * getters and setters
	 * @param <E>
	 */
    private static class Node<E>{
        private E element;
        private Node<E> next, previous;
        public Node(E element, Node<E> next, Node<E> previous){
            this.element=element;
            this.next=next;
            this.previous=previous;
        }
        public Node(){
            this.element=null;
            this.next=null;
            this.previous=null;
        }
        public E getElement(){
            return element;
        }
        public void setElement(E element){
            this.element=element;
        }
        public Node<E> getNext(){
            return next;
        }
        public Node<E> getPrevious(){
            return previous;
        }
        public void setNext(Node<E> next){
            this.next=next;
        }
        public void setPrevious(Node<E> previous) {
            this.previous = previous;
        }
    }
    
    private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E> {
        private Node<E> nextNode;

        public SortedCircularDoublyLinkedListIterator(){
            this.nextNode = (Node<E>) header.getNext();
        }
        @Override
        public boolean hasNext() {
            return this.nextNode.getElement()!=null; 
        }

        @Override
        public E next() {
            if (this.hasNext()) {
                E result = this.nextNode.getElement();
                this.nextNode = this.nextNode.getNext();
                return result;
            }
            else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {

        }
    }


    private Node<E> header;
    private int currentSize;
    private Comparator<E> comparator;
    
    public SortedCircularDoublyLinkedList(Comparator<E> c) {
        this.header= new Node<E>();
        this.header.setNext(this.header);
        this.header.setPrevious(this.header);
        this.currentSize=0;
        this.comparator=c;

    }
    /**
	 * @author kelminator
	 *The function public boolean add(E obj) adds a object E into the list 
	 *First it checks if the list is empty, if so then just add it and set the 
	 *next and prev to the header. Then, I walk through the list should the target object
	 *be in middle of the list, aka inside the loop, I create a new node
	 * and add it behind the object of the list I am on, temp, because of the way I compare them. 
	 * If the loop completes that means there are no larger objects than the target. 
	 * So I created a new node and placed it before the header
	 * @param <E>
	 */
    @Override
    public boolean add(E obj) {
        if(this.isEmpty()){
        	Node<E> newNode = new Node<E>(obj, this.header, this.header);
            this.header.setNext(newNode);
            this.header.setPrevious(newNode);
            currentSize++;
            return true;
        }
        else{
        	Node<E> temp = header.getNext();
        	while(temp!=this.header) {//changed this from temp.getElement!=null
        		if(this.comparator.compare(obj, temp.getElement())<=0) {
        			Node<E> newNode = new Node<E>(obj, temp, temp.getPrevious());
        			temp.getPrevious().setNext(newNode);
        			temp.setPrevious(newNode);
        			currentSize++;
        			return true;
        		}
        		temp=temp.getNext();
        	}
        	Node<E> newNode = new Node<E>(obj, this.header, this.header.getPrevious());
        	this.header.getPrevious().setNext(newNode);
        	this.header.setPrevious(newNode);
        	currentSize++;
        	return true;
        }
    }

    @Override
    public int size() {
        return this.currentSize;
    }

    @Override
    public boolean remove(E obj) {
        int i = this.firstIndex(obj);
        if(i<0) {
            return false;
        }
        else{
            this.remove(i);
            return true;
        }
    }

    @Override
    public boolean remove(int index) {
        if(index<0 || index>=this.currentSize){
            throw new IndexOutOfBoundsException();
        }
        Node<E> temp = this.header;
        Node<E> target = null;
        int currentPos = 0;
        while(currentPos!=index){
        	temp = temp.getNext();
            currentPos++;
        }
        target = temp.getNext();
        temp.setNext(target.getNext());
        target.getNext().setPrevious(temp);
        target.setPrevious(null);
        target.setNext(null);
        target.setElement(null);
        target = null;
        currentSize--;
        return true;
    }

    @Override
    public int removeAll(E obj) {
        int count=0;
        while(this.remove(obj)){
            count++;
        }
        return count;
    }

    @Override
    public E first() {
        return(this.isEmpty() ? null: this.header.getNext().getElement());
    }

    @Override
    public E last() {
        return (this.isEmpty() ? null:this.header.getPrevious().getElement());
    }

    @Override
    public E get(int index) {
        int currentPos =0;
        Node<E> temp = this.header.getNext();
        while(currentPos!=index){
            temp = temp.getNext();
            currentPos++;
        }
        return temp.getElement();
    }

    @Override
    public void clear() {
    while(!this.isEmpty()){
        this.remove(0);
    }
    }

    @Override
    public boolean contains(E integer) {
        return firstIndex(integer)!=-1;
    }

    @Override
    public boolean isEmpty() {
        return this.currentSize==0;
    }

    @Override
    public int firstIndex(E integer) {
        int i = 0;
        for(Node<E> temp = this.header.getNext(); temp!=this.header; temp=temp.getNext(), i++){ //changed from temp.getElement != null
            if((temp.getElement().equals(integer))){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndex(E integer) {
        int i = this.currentSize-1;
        for(Node<E> temp = this.header.getPrevious(); temp!=this.header; temp=temp.getPrevious(), i--){ //changed from temp.getElement != null
            if((temp.getElement().equals(integer))){
                return i;
            }
        }
        return -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new SortedCircularDoublyLinkedListIterator<E>();
    }


}


