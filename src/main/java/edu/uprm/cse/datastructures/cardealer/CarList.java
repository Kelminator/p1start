package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;

public class CarList {
	
	private static CarList singleton = new CarList();
	private static SortedCircularDoublyLinkedList<Car> cars = new SortedCircularDoublyLinkedList<Car>(new CarComparator());
	
	public CarList() {
		
	}
	public static CarList getinstance(){
		return singleton;
	}
	public static void resetCars(){
		cars.clear();
	}
	public SortedCircularDoublyLinkedList<Car> getCars(){
		return cars;
	}


}
