package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Path;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private static SortedCircularDoublyLinkedList<Car> cars =  CarList.getinstance().getCars();


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] carsToArray() {
		Car[] carArray = new Car[cars.size()];
		int i = 0;
		for(Car car: cars){
			carArray[i] = car;
			i++;
		}
		return carArray;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for(Car car: cars){
			if(car.getCarId() == id){
				return car;
			}
		}

		throw new NotFoundException();

	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar){
		for(int i = 0; i< cars.size(); i++){
			if(cars.get(i).getCarId() == newCar.getCarId()){
				return Response.status(Response.Status.FORBIDDEN).build();
			}

		}
		cars.add(newCar);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car newcar){
		for(Car car: cars){
			if(car.getCarId() == newcar.getCarId()){
				cars.remove(car);
				cars.add(newcar);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		boolean removed = false;
		for (Car car: cars) {
			if(car.getCarId()== id){
				cars.remove(car);
				removed = true;
				break;
			}
				
		}
		if(!removed) {
			throw new NotFoundException();	
		}
		return Response.status(Response.Status.OK).build();
	}
}

